package com.kshrd.multilingual;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.akexorcist.localizationactivity.LocalizationActivity;

public class LoginActivity extends LocalizationActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }
}
