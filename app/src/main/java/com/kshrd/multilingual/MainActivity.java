package com.kshrd.multilingual;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.akexorcist.localizationactivity.LocalizationActivity;

import static com.akexorcist.localizationactivity.LanguageSetting.setDefaultLanguage;

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setDefaultLanguage("km");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_language, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.lang_en){
            setLanguage("en");
        } else if (item.getItemId() == R.id.lang_kh){
            setLanguage("km");
        } else if (item.getItemId() == R.id.lang_vn){
            setLanguage("vi");
        }
        return super.onOptionsItemSelected(item);
    }


}
